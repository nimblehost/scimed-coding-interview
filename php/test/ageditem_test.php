<?php

class AgedItemText extends PHPUnit_Framework_TestCase
{
	public function testGetName()
	{
		$item = new \SciMed\AgedItem('aged', 10, 10);
		$expect = 'aged';
		$this->assertSame($expect, $item->getName());
	}

	public function testSetName()
	{
		$item = new \SciMed\AgedItem('aged', 10, 10);
		$item->setName('newname');
		$expect = 'newname';
		$this->assertSame($expect, $item->getName());
	}

	public function testGetSellIn()
	{
		$item = new \SciMed\AgedItem('aged', 20, 10);
		$expect = 20;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testSetSellIn()
	{
		$item = new \SciMed\AgedItem('aged', 20, 10);
		$item->setSellIn(10);
		$expect = 10;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testGetQuality()
	{
		$item = new \SciMed\AgedItem('aged', 10, 10);
		$expect = 10;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testSetQuality()
	{
		$item = new \SciMed\AgedItem('aged', 10, 10);
		$item->setQuality(5);
		$expect = 5;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQuality()
	{
		$item = new \SciMed\AgedItem('aged', 0, 0);
		$item->update_quality();
		$expect = 1;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityMax()
	{
		$item = new \SciMed\AgedItem('aged', 10, 50);
		$item->update_quality();
		$expect = 50;
		$this->assertEquals($expect, $item->getQuality());
	}
}