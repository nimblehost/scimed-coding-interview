<?php

include dirname(__DIR__) . '/src/Autoloader.php';
$autoloader = new \SciMed\Autoloader();

spl_autoload_register(array($autoloader, 'load'));