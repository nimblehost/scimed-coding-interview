<?php

class LegendaryText extends PHPUnit_Framework_TestCase
{
	public function testGetName()
	{
		$item = new \SciMed\LegendaryItem('legendary', 10, 10);
		$expect = 'legendary';
		$this->assertSame($expect, $item->getName());
	}

	public function testSetName()
	{
		$item = new \SciMed\LegendaryItem('legendary', 10, 10);
		$item->setName('newname');
		$expect = 'newname';
		$this->assertSame($expect, $item->getName());
	}

	public function testGetSellIn()
	{
		$item = new \SciMed\LegendaryItem('legendary', -1, 10);
		$expect = -1;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testSetSellIn()
	{
		$item = new \SciMed\LegendaryItem('legendary', 20, 10);
		$item->setSellIn(10);
		$expect = 10;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testGetQuality()
	{
		$item = new \SciMed\LegendaryItem('legendary', 10, 10);
		$expect = 80;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testSetQuality()
	{
		$item = new \SciMed\LegendaryItem('legendary', 10, 10);
		$item->setQuality(5);
		$expect = 80;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQuality()
	{
		$item = new \SciMed\LegendaryItem('legendary', 20, 10);
		$item->update_quality();
		$expect = 80;
		$this->assertEquals($expect, $item->getQuality());
	}
	
}