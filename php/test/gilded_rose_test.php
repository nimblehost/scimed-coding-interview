<?php

class GildedRoseTest extends PHPUnit_Framework_TestCase {

    public function testRunInventoryStandardNormal() {
        $items = array(new \SciMed\StandardItem("foo", 1, 1));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(0, $items[0]->getQuality());
    }

    public function testRunInventoryStandardFast() {
        $items = array(new \SciMed\StandardItem("foo", -1, 2));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(0, $items[0]->getQuality());
    }

    public function testRunInventoryAged() {
        $items = array(new \SciMed\AgedItem("foo", 0, 0));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(1, $items[0]->getQuality());
    }

    public function testRunInventoryAgedExpiresNormal() {
        $items = array(new \SciMed\AgedExpiresItem("foo", 20, 0));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(1, $items[0]->getQuality());
    }

    public function testRunInventoryAgedExpiresMedium() {
        $items = array(new \SciMed\AgedExpiresItem("foo", 9, 0));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(2, $items[0]->getQuality());
    }
    public function testRunInventoryAgedExpiresFast() {
        $items = array(new \SciMed\AgedExpiresItem("foo", 4, 0));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(3, $items[0]->getQuality());
    }
    public function testRunInventoryAgedExpiresExpired() {
        $items = array(new \SciMed\AgedExpiresItem("foo", 0, 50));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(0, $items[0]->getQuality());
    }

    public function testRunInventoryLegendary() {
        $items = array(new \SciMed\LegendaryItem("foo", -1, 50));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(80, $items[0]->getQuality());
    }

    public function testRunInventoryConjuredNormal() {
        $items = array(new \SciMed\ConjuredItem("foo", 1, 2));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(0, $items[0]->getQuality());
    }

    public function testRunInventoryConjuredFast() {
        $items = array(new \SciMed\ConjuredItem("foo", -1, 4));
        $gildedRose = new \SciMed\GildedRose($items);
        $gildedRose->runInventory();
        $this->assertEquals(0, $items[0]->getQuality());
    }

}
