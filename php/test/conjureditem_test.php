<?php

class ConjuredText extends PHPUnit_Framework_TestCase
{
	public function testGetName()
	{
		$item = new \SciMed\ConjuredItem('conjured', 10, 10);
		$expect = 'conjured';
		$this->assertSame($expect, $item->getName());
	}

	public function testSetName()
	{
		$item = new \SciMed\ConjuredItem('conjured', 10, 10);
		$item->setName('newname');
		$expect = 'newname';
		$this->assertSame($expect, $item->getName());
	}

	public function testGetSellIn()
	{
		$item = new \SciMed\ConjuredItem('conjured', 20, 10);
		$expect = 20;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testSetSellIn()
	{
		$item = new \SciMed\ConjuredItem('conjured', 20, 10);
		$item->setSellIn(10);
		$expect = 10;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testGetQuality()
	{
		$item = new \SciMed\ConjuredItem('conjured', 10, 10);
		$expect = 10;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testSetQuality()
	{
		$item = new \SciMed\ConjuredItem('conjured', 10, 10);
		$item->setQuality(5);
		$expect = 5;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityNormal()
	{
		$item = new \SciMed\ConjuredItem('conjured', 10, 10);
		$item->update_quality();
		$expect = 8;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityFast()
	{
		$item = new \SciMed\ConjuredItem('conjured', 0, 10);
		$item->update_quality();
		$expect = 6;
		$this->assertEquals($expect, $item->getQuality());
	}
	
}