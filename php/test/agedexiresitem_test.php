<?php

class AgedExpiresItemTest extends PHPUnit_Framework_TestCase
{
	public function testGetName()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 10, 10);
		$expect = 'agedexpires';
		$this->assertSame($expect, $item->getName());
	}

	public function testSetName()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 10, 10);
		$item->setName('newname');
		$expect = 'newname';
		$this->assertSame($expect, $item->getName());
	}

	public function testGetSellIn()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 20, 10);
		$expect = 20;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testSetSellIn()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 20, 10);
		$item->setSellIn(10);
		$expect = 10;
		$this->assertSame($expect, $item->getSellIn());
	}

	public function testGetQuality()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 10, 10);
		$expect = 10;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testSetQuality()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 10, 10);
		$item->setQuality(5);
		$expect = 5;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityNormal()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 20, 10);
		$item->update_quality();
		$expect = 11;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityMedium()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 9, 10);
		$item->update_quality();
		$expect = 12;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityFast()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 4, 10);
		$item->update_quality();
		$expect = 13;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityExpired()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 0, 10);
		$item->update_quality();
		$expect = 0;
		$this->assertEquals($expect, $item->getQuality());
	}

	public function testUpdateQualityMax()
	{
		$item = new \SciMed\AgedExpiresItem('agedexpires', 3, 50);
		$item->update_quality();
		$expect = 50;
		$this->assertEquals($expect, $item->getQuality());
	}	
}