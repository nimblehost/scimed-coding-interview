<?php
namespace SciMed;

/*
 * Very simple autoload class
 */
class Autoloader
{
	/*
	 * Simply looks for a named file which matches the class called.
	 * For a more complex app this should be more robust to
	 * cover nested namespace separators, etc.
	 */
	public function load($class)
	{
		// Don't autoload PHPUnit classes, they have their own autoloader
		if (strpos($class, 'PHP') === false) {
			
			// Trim the leading namespace character
			$class = ltrim($class, '\\');

			// Replace namespace separator so $class maps to the correct path
			$class = str_replace('\\', DIRECTORY_SEPARATOR, $class);

			require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . $class . '.php';
			
		}
	}
}