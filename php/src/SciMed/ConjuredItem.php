<?php
namespace SciMed;

/**
 * A Conjured Item class for the Gilded Rose Inn
 * Since this class extends StandardItem, it has access
 * to its constants and the Updatable interface required methods
 */
class ConjuredItem extends StandardItem
{

	/**
	 * Update the quality of the Conjured Item
	 */
	public function update_quality()
	{
		// Item quality cannot be below zero
		if ($this->quality > self::QUALITY_MIN) {
			if ($this->sell_in > 0) {
				$this->quality -= (self::QUALITY_SPEED_NORMAL * 2);
			} else {
				$this->quality -= (self::QUALITY_SPEED_FAST * 2);
			}
		}

		// Quality could have been set below minimum, check and reset if necessary
		$this->checkQualityThreshold();
	}

}