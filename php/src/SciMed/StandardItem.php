<?php
namespace SciMed;

/**
 * A Standard Item class for the Gilded Rose Inn
 */
class StandardItem extends GRItem implements Updatable
{
	/**
	 * @const integer
	 */
	const QUALITY_SPEED_NORMAL = 1;

	/**
	 * @const integer
	 */
	const QUALITY_SPEED_FAST = 2;

	/**
	 * Update the quality of the Item
	 */
	public function update_quality()
	{
		// Item quality cannot be below zero
		if ($this->quality > self::QUALITY_MIN) {
			// Quality degrades normally before the sell_in date, faster afterwards
			if ($this->sell_in > 0) {
				$this->quality -= self::QUALITY_SPEED_NORMAL;
			} else {
				$this->quality -= self::QUALITY_SPEED_FAST;
			}
		}

		// Quality could have been set below minimum, check and reset if necessary
		$this->checkQualityThreshold();
	}

	/**
	 * Update the sell_in value of the Item
	 */
	public function update_sellIn()
	{
		$this->sell_in -= self::SELLIN_SPEED;
	}
}