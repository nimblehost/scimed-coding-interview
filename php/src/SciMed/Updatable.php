<?php
namespace SciMed;

/**
 * The Updatable interface
 */
interface Updatable
{
	public function update_quality();

	public function update_sellIn();
}