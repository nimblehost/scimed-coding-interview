<?php
namespace SciMed;

/**
 * A AgedExpires Item class for the Gilded Rose Inn
 */
class AgedExpiresItem extends GRItem implements Updatable
{
	/**
	 * @const integer
	 */
	const QUALITY_SPEED_NORMAL = 1;

	/**
	 * @const integer
	 */
	const QUALITY_SPEED_MEDIUM = 2;

	/**
	 * @const integer
	 */
	const QUALITY_SPEED_FAST = 3;

	/**
	 * @const integer
	 */
	const QUALITY_THRESHOLD_MEDIUM = 10;

	/**
	 * @const integer
	 */
	const QUALITY_THRESHOLD_FAST = 5;

	/**
	 * @const integer
	 */
	const QUALITY_THRESHOLD_EXPIRES = 0;

	/**
	 * Update the quality of the Item
	 */
	public function update_quality()
	{
		// Quality increases before the sell_in date
		if ($this->sell_in > self::QUALITY_THRESHOLD_MEDIUM) {
			// Increase is normal until first threshold
			$this->quality += self::QUALITY_SPEED_NORMAL;
		} elseif ($this->sell_in > self::QUALITY_THRESHOLD_FAST) {
			// Increase is greater until second threshold
			$this->quality += self::QUALITY_SPEED_MEDIUM;
		} elseif ($this->sell_in > self::QUALITY_THRESHOLD_EXPIRES) {
			// Increase is fastest just before expiration
			$this->quality += self::QUALITY_SPEED_FAST;
		} else {
			// Quality is zero at expiration
			$this->quality = self::QUALITY_MIN;
		}

		// Quality could have been set above maximum, check and reset if necessary
		$this->checkQualityThreshold();
	}

	/**
	 * Update the sell_in value of the Item
	 */
	public function update_sellIn()
	{
		$this->sell_in -= self::SELLIN_SPEED;
	}
}