<?php
namespace SciMed;

/**
 * The main class for the Gilded Rose Inn
 */
class GildedRose {

    /**
     * @var array
     */
    private $items;

    /**
     * Constructor
     * @param array An array of GRItems
     */
    public function __construct(array $items) {
        $this->items = $items;
    }

    /**
     * Loops through the $items array to update the $quality and $sell_in
     * properties of each item
     */
    public function runInventory()
    {
        foreach ($this->items as $item) {
            $item->update_quality();
            $item->update_sellIn();
        }
        
    }

}


