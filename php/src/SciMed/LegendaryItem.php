<?php
namespace SciMed;

/**
 * A Legendary Item class for the Gilded Rose Inn
 */
class LegendaryItem extends GRItem implements Updatable
{
	/**
	 * @const integer
	 */
	const QUALITY = 80;

	/**
	 * Constructor
	 * @param string
	 * @param integer
	 * @param integer
	 */
	public function __construct($name, $sell_in, $quality)
	{
		$this->name = $name;
        $this->sell_in = $sell_in;
        // Legendary items have a fixed quality that does not change
        $this->quality = $quality != self::QUALITY ? self::QUALITY : $quality;
	}

	/**
	 * Update the quality of the Item
	 * This overrides the parent class' setQuality() method
	 */
	public function setQuality($quality)
	{
		// Legendary items have quality that remains constant at the value specified in self::QUALITY
		// It should not be changed to another value
		$this->quality = $quality != self::QUALITY ? self::QUALITY : $quality;
	}

	/**
	 * Update the quality of the Item
	 */
	public function update_quality()
	{
		// Legendary items have quality that remains constant at the value specified in self::QUALITY
		if ($this->quality != self::QUALITY) {
			$this->quality = self::QUALITY;
		}
	}

	/**
	 * Update the sell_in value of the Item
	 */
	public function update_sellIn()
	{
		// Legendary items do not have to be sold, thus $sell_in does not need updating
	}
}