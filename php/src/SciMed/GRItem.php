<?php
namespace SciMed;

/**
 * Abstract GRItem class for Gilded Rose which should not be implemented directly.
 * Instead use the classes which extend GRItem, such as StandardItem, LegendaryItem,
 * AgedItem, AgedExpiresItem, and ConjuredItem, depending on the characteristics
 * of the item to be created. Or create a new class which extends
 * the abstract Item class with the characteristics needed.
 */
abstract class GRItem implements Updatable {

    /**
     * @const
     */
    const QUALITY_MIN = 0;

    /**
     * @const
     */
    const QUALITY_MAX = 50;

    /**
     * @const integer
     */
    const SELLIN_SPEED = 1;

    /**
     * @var string
     */ 
    protected $name;

    /**
     * @var integer
     */
    protected $sell_in;

    /**
     * @var integer
     */
    protected $quality;

    /**
     * Constructor
     * @param string $name The name of the Item
     * @param integer $sell_in The number of days an Item should be sold by
     * @param integer $quality The quality value of the Item
     */
    public function __construct($name, $sell_in, $quality) {
        $this->name = $name;
        $this->sell_in = $sell_in;
        // The quality of an item cannot be below self::QUALITY_MIN
        $this->quality = $quality < self::QUALITY_MIN ? self::QUALITY_MIN : $quality;
    }

    /**
     * Get the name of the item
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the name of the item
     * @param string $name The name of the Item
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get the sell_in value of the item
     * @return integer
     */
    public function getSellIn()
    {
        return $this->sell_in;
    }

    /**
     * Set Item sell_in value
     * @param integer $sell_in The number of days in which the item should be sold
     */
    public function setSellIn($sellIn)
    {
        $this->sell_in = $sellIn;
    }

    /**
     * Get the quality of the item
     * @return integer
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set the quality of the item
     * @param integer $quality The quality value of the Item
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    }

    /**
     * Check quality thresholds and reset if necessary
     */
    public function checkQualityThreshold()
    {
        // Item quality can't be below the minimum
        if ($this->quality < self::QUALITY_MIN) {
            $this->quality = self::QUALITY_MIN;
        }

        // Item quality can't be above the maximum
        if ($this->quality > self::QUALITY_MAX) {
            $this->quality = self::QUALITY_MAX;
        }        
        
    }

    /**
     * Display an Item's properties as strings
     */
    public function __toString() {
        return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }

    /**
     * Declare the update_quality() function required as part
     * of the Updatable interface. This is just a stub as the
     * logic for this function will vary depending on the item.
     */
    public function update_quality()
    {
        
    }

    /**
     * Declare the update_sellIn() function required as part
     * of the Updatable interface. This is just a stub as the
     * logic for this function will vary depending on the item.
     */
    public function update_sellIn()
    {
        
    }

}
