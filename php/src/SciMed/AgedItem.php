<?php
namespace SciMed;

/**
 * A Aged Item class for the Gilded Rose Inn
 */
class AgedItem extends GRItem implements Updatable
{
	/**
	 * @const integer
	 */
	const QUALITY_SPEED = 1;

	/**
	 * Update the quality of the Item
	 */
	public function update_quality()
	{
		// Aged Item quality cannot be greater than the max
		if ($this->quality < self::QUALITY_MAX) {
			$this->quality += self::QUALITY_SPEED;
		}

		// Quality could have been set above maximum, check and reset if necessary
		$this->checkQualityThreshold();
	}

	/**
	 * Update the sell_in value of the Item
	 */
	public function update_sellIn()
	{
		$this->sell_in -= self::SELLIN_SPEED;
	}
}