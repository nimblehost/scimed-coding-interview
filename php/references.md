## Reference Materials Used For Exercise

### Coding and comments style
- http://www.php-fig.org/psr/
- https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md
- https://github.com/codeguy/Slim/blob/2.x/Slim/Slim.php

### Type hinting
- http://php.net/manual/en/functions.arguments.php#functions.arguments.type-declaration
- http://stackoverflow.com/questions/778564/phpdoc-type-hinting-for-array-of-objects

### PHP Interfaces
- Modern PHP by Josh Lockhart, published by O'Reilly, pages 13-15

### $argv
- http://php.net/manual/en/reserved.variables.argv.php